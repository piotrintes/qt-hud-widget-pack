#include "dialwidget.h"
#include <QPainter>
#include <QtMath>


int Dial::getActAngle() const
{
    return actAngle;
}

QWidget *Dial::getParent() const
{
    return parent;
}

void Dial::setParent(QWidget *value)
{
    parent = value;
    if (parent != nullptr) parent->repaint();
}

bool Dial::getFilled() const
{
    return filled;
}

void Dial::setFilled(bool value)
{
    filled = value;
}

int Dial::getStartAngle() const
{
    return startAngle;
}

void Dial::setStartAngle(int value)
{
    startAngle = value;
    if (parent != nullptr) parent->repaint();
}

int Dial::getEndAngle() const
{
    return endAngle;
}

void Dial::setEndAngle(int value)
{
    endAngle = value;
    if (parent != nullptr) parent->repaint();
}

int Dial::getMinVal() const
{
    return minVal;
}

void Dial::setMinVal(int value)
{
    minVal = value;
    if (parent != nullptr) parent->repaint();
}

int Dial::getMaxVal() const
{
    return maxVal;
}

void Dial::setMaxVal(int value)
{
    maxVal = value;
    if (parent != nullptr) parent->repaint();
}

int Dial::getVal() const
{
    return val;
}

void Dial::setVal(int value)
{
    val = value;
    if (val > maxVal) val = maxVal;
    if (val < minVal) val = minVal;
    int angleRange = endAngle - startAngle;
    int valRange = maxVal - minVal;
    actAngle = (val - minVal) * angleRange/valRange;
    emit valueChanged();
    if (parent != nullptr) parent->repaint();
}

void Dial::setValFromAngle(int value)
{
     actAngle = value - startAngle;
     int angleRange = endAngle - startAngle;
     int valRange = maxVal - minVal;
     setVal(actAngle * valRange / angleRange + minVal);
}

QColor Dial::getColor() const
{
    return color;
}

void Dial::setColor(const QColor &value)
{
    color = value;
    if (parent != nullptr) parent->repaint();
}




int DialWidget::getDialWidth() const
{
    return dialWidth;
}

void DialWidget::setDialWidth(int value)
{
    dialWidth = value;
}

void DialWidget::addDial()
{
    Dial *dial = new Dial();
    dial->setParent(this);
    dials->append(dial);
}

void DialWidget::setDisplaysVisible(bool title, bool undertext, bool value)
{
    displayTitle = title;
    displayValue = value;
    displayUndertext = undertext;
}

int DialWidget::getValue() const
{
    return value;
}

void DialWidget::setValue(int value)
{
    this->value = value;
    repaint();
}

QString DialWidget::getTitle() const
{
    return title;
}

void DialWidget::setTitle(const QString &value)
{
    title = value;
    repaint();
}

QString DialWidget::getUndertext() const
{
    return undertext;
}

void DialWidget::setUndertext(const QString &value)
{
    undertext = value;
    repaint();
}

bool DialWidget::getDisplayValue() const
{
    return displayValue;
}

void DialWidget::setDisplayValue(bool value)
{
    displayValue = value;
    repaint();
}

bool DialWidget::getDisplayTitle() const
{
    return displayTitle;
}

void DialWidget::setDisplayTitle(bool value)
{
    displayTitle = value;
    repaint();
}

bool DialWidget::getDisplayUndertext() const
{
    return displayUndertext;
}

void DialWidget::setDisplayUndertext(bool value)
{
    displayUndertext = value;
    repaint();
}

QString DialWidget::getValAdd() const
{
    return valAdd;
}

void DialWidget::setValAdd(const QString &value)
{
    valAdd = value;
    repaint();
}

int DialWidget::getTickInterval() const
{
    return tickInterval;
}

void DialWidget::setTickInterval(int value)
{
    tickInterval = value;
    repaint();
}

int DialWidget::getTickBigInterval() const
{
    return tickBigInterval;
}

void DialWidget::setTickBigInterval(int value)
{
    tickBigInterval = value;
    repaint();
}

int DialWidget::getTickStartAngle() const
{
    return tickStartAngle;
}

void DialWidget::setTickStartAngle(int value)
{
    tickStartAngle = value;
    repaint();
}

int DialWidget::getTickEndAngle() const
{
    return tickEndAngle;
}

void DialWidget::setTickEndAngle(int value)
{
    tickEndAngle = value;
    repaint();
}

float DialWidget::getTickAngleToVal() const
{
    return tickAngleToVal;
}

void DialWidget::setTickAngleToVal(float value)
{
    tickAngleToVal = value;
}

bool DialWidget::getTop0() const
{
    return top0;
}

void DialWidget::setTop0(bool value)
{
    top0 = value;
}

DialWidget::DialWidget(QWidget* parent): QWidget(parent)
{}

void DialWidget::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.setRenderHints(QPainter::Antialiasing, true);
    QPen penGlobal;
    penGlobal.setColor(palette().color(QPalette::WindowText));
    QPen penDial;
    penDial.setWidth(dialWidth);
    penDial.setCapStyle(Qt::FlatCap);
    QPen penTick;
    penTick.setWidth(dialWidth/5);
    penTick.setCapStyle(Qt::FlatCap);
    QPen penBigTick;
    penBigTick.setWidth(dialWidth/2);
    penBigTick.setCapStyle(Qt::FlatCap);

    //setup variables
    int size = width()-5;
    if (size > height()-5) size = height()-5;
    int x = width()/2 -size/2;
    int y = height()/2 -size/2;

    //Draw dials
    for (int i = 0; i < dials->length(); i++) {
        Dial* dial = dials->at(i);
        penDial.setColor(dial->getColor());
        painter.setPen(penDial);
        if(dial->getFilled()) {
            if(top0)
                painter.drawArc(x+dialWidth/2,y+dialWidth/2,size-dialWidth,size-dialWidth,-dial->getStartAngle()*16+90*16,-dial->getActAngle()*16);
            else
                painter.drawArc(x+dialWidth/2,y+dialWidth/2,size-dialWidth,size-dialWidth,-dial->getStartAngle()*16-90*16,-dial->getActAngle()*16);
        } else {
            if(top0)
                painter.drawArc(x+dialWidth/2,y+dialWidth/2,size-dialWidth,size-dialWidth,-(dial->getStartAngle() + dial->getActAngle())*16+90*16-25,40);
            else
                painter.drawArc(x+dialWidth/2,y+dialWidth/2,size-dialWidth,size-dialWidth,-(dial->getStartAngle() + dial->getActAngle())*16-90*16-25,40);
        }
    }

    //Draw ticks
    if (tickInterval > 0) {
        painter.setPen(penTick);
        for (int i = tickStartAngle; i<=tickEndAngle; i=i+tickInterval)
            if(top0)
                painter.drawArc(x+(dialWidth/5)/2,y+(dialWidth/5)/2,size-(dialWidth/5),size-(dialWidth/5),-i*16+90*16-10,10);
            else
                painter.drawArc(x+(dialWidth/5)/2,y+(dialWidth/5)/2,size-(dialWidth/5),size-(dialWidth/5),-i*16-90*16-10,10);
    }
    if (tickBigInterval > 0) {
        painter.setPen(penBigTick);
        for (int i = tickStartAngle; i<=tickEndAngle; i=i+tickBigInterval){
            if(top0)
                painter.drawArc(x+(dialWidth/2)/2,y+(dialWidth/2)/2,size-(dialWidth/2),size-(dialWidth/2),-i*16+90*16-20,20);
            else
                painter.drawArc(x+(dialWidth/2)/2,y+(dialWidth/2)/2,size-(dialWidth/2),size-(dialWidth/2),-i*16-90*16-20,20);
        }
    }


    painter.setPen(penGlobal);
    //Draw external circle
    painter.drawEllipse(x,y,size,size);


    //painter.drawArc(x+dialWidth/2,y+dialWidth/2,size-dialWidth,size-dialWidth,atan2(pressX-(x+size/2),pressY-(y+size/2))*180/3.14*16-90*16,160);

    //Draw internal circle
    painter.drawEllipse(x+dialWidth,y+dialWidth,size-dialWidth*2,size-dialWidth*2);

    //Draw internal text
    if (displayTitle)
        painter.drawText(x+dialWidth*3/2,y+dialWidth,size-dialWidth*3,size/3,Qt::AlignCenter,title);
    if (displayUndertext)
        painter.drawText(x+dialWidth*3/2,y+dialWidth+size/3,size-dialWidth*3,size-dialWidth*4,Qt::AlignCenter,undertext);
    if (displayValue) {
        QFont f = painter.font();
        f.setPointSize(f.pointSize()*2);
        painter.setFont(f);
        painter.drawText(x+dialWidth,y+dialWidth,size-dialWidth*2,size-dialWidth*2,Qt::AlignCenter,QString::number(value) + valAdd);
    }
}

void DialWidget::mousePressEvent(QMouseEvent * event)
{
    QWidget::mousePressEvent(event);     // this will do base class content
    isClicked=true;
    //setup variables
    int size = width()-5;
    if (size > height()-5) size = height()-5;
    int x = width()/2 -size/2;
    int y = height()/2 -size/2;
    pressX = event->x();
    pressY = event->y();

    if(movableDial != nullptr)
        if(top0)
            movableDial->setValFromAngle(atan2(-pressX+(x+size/2),pressY-(y+size/2))*180/3.14+180);
        else
            movableDial->setValFromAngle(atan2(pressX-(x+size/2),-pressY+(y+size/2))*180/3.14+180);
    repaint();
    update();
}

void DialWidget::mouseReleaseEvent(QMouseEvent * event)
{
    QWidget::mouseReleaseEvent(event);     // this will do base class content
    isClicked=false;
    pressX = event->x();
    pressY = event->y();
    update();
}

void DialWidget::mouseMoveEvent(QMouseEvent *event)
{
    QWidget::mouseMoveEvent(event);     // this will do base class content
    if(isClicked)
    {
        //setup variables
        int size = width()-5;
        if (size > height()-5) size = height()-5;
        int x = width()/2 -size/2;
        int y = height()/2 -size/2;
        pressX = event->x();
        pressY = event->y();

        if(movableDial != nullptr)
            if(top0)
                movableDial->setValFromAngle(atan2(-pressX+(x+size/2),pressY-(y+size/2))*180/3.14+180);
            else
                movableDial->setValFromAngle(atan2(pressX-(x+size/2),-pressY+(y+size/2))*180/3.14+180);
        repaint();
    }
}
