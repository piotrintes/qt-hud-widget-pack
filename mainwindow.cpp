#include "mainwindow.h"
#include "ui_mainwindow.h"

int lastVal = 0;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->widget->setTitle("Dial");
    ui->widget->setUndertext("val/100");
    ui->widget->setValAdd(" v");
    ui->widget->addDial();
    ui->widget->dials->at(0)->setMinVal(ui->horizontalSlider->minimum());
    ui->widget->dials->at(0)->setMaxVal(ui->horizontalSlider->maximum());
    ui->widget->dials->at(0)->setColor(QColor(255,100,70));
    ui->widget->dials->at(0)->setStartAngle(10);
    ui->widget->dials->at(0)->setEndAngle(170);
    ui->widget->addDial();
    ui->widget->dials->at(1)->setColor(QColor(70,100,255));
    ui->widget->dials->at(1)->setStartAngle(-10);
    ui->widget->dials->at(1)->setEndAngle(-170);
    ui->widget->addDial();
    ui->widget->dials->at(2)->setColor(QColor(100,255,70));
    ui->widget->dials->at(2)->setMaxVal(ui->horizontalSlider_3->maximum());
    ui->widget->movableDial = ui->widget->dials->at(0);
    connect(ui->widget->movableDial, &Dial::valueChanged,[this]() {
        ui->widget->setValue(ui->widget->movableDial->getVal());
        ui->horizontalSlider->setValue(ui->widget->movableDial->getVal());
    });



    ui->widget_2->setTitle("Speed");
    ui->widget_2->setUndertext("km/h");
    ui->widget_2->setTickInterval(10);
    ui->widget_2->setTickBigInterval(40);
    ui->widget_2->setTickStartAngle(40);
    ui->widget_2->setTickEndAngle(320);
    ui->widget_2->addDial();
    ui->widget_2->dials->at(0)->setMaxVal(ui->horizontalSlider_4->maximum());
    ui->widget_2->dials->at(0)->setColor(QColor(255,100,70));
    ui->widget_2->dials->at(0)->setStartAngle(40);
    ui->widget_2->dials->at(0)->setEndAngle(320);
    ui->widget_2->dials->at(0)->setFilled(false);
    ui->widget_2->addDial();
    ui->widget_2->dials->at(1)->setColor(QColor(255,100,70,50));
    ui->widget_2->dials->at(1)->setStartAngle(240);
    ui->widget_2->dials->at(1)->setEndAngle(320);
    ui->widget_2->dials->at(1)->setVal(100);



    ui->compass_0->setDisplaysVisible(false,false,false);
    ui->compass_0->setTickInterval(10);
    ui->compass_0->setTickBigInterval(90);
    ui->compass_0->setTop0(true);

    ui->compass_0->addDial();
    ui->compass_0->dials->at(0)->setColor(QColor(255,100,70));
    ui->compass_0->dials->at(0)->setMaxVal(0);
    ui->compass_0->dials->at(0)->setFilled(false);
    ui->compass_0->dials->at(0)->setStartAngle(0);
    ui->compass_0->addDial();
    ui->compass_0->dials->at(1)->setColor(QColor(70,100,255));
    ui->compass_0->dials->at(1)->setMaxVal(360);
    ui->compass_0->dials->at(1)->setFilled(false);
    ui->compass_0->dials->at(1)->setStartAngle(0);
    ui->compass_0->dials->at(1)->setEndAngle(360);
    ui->compass_0->movableDial = ui->compass_0->dials->at(1);
    connect(ui->compass_0->movableDial, &Dial::valueChanged,[this]() {

        ui->newHeading->setValue(ui->compass_0->movableDial->getVal());

        int globalangle = ui->compass_0->movableDial->getVal()-ui->compass_0->dials->at(0)->getStartAngle();

        if (globalangle > 360) globalangle = globalangle -360;
        if (globalangle < 0) globalangle = globalangle +360;

        ui->newHeading_2->setValue(globalangle);


        if (ui->newHeading->value() < 180) {
            ui->compass_1->dials->at(0)->setEndAngle(ui->newHeading->value());
            ui->compass_1->dials->at(0)->setStartAngle(0);
        } else {
            ui->compass_1->dials->at(0)->setStartAngle(ui->newHeading->value());
            ui->compass_1->dials->at(0)->setEndAngle(360);
        }

        ui->compass_1->dials->at(0)->setVal(100);
    });

    ui->compass_1->setDisplaysVisible(false,false,false);
    ui->compass_1->setDialWidth(10);
    ui->compass_1->setTop0(true);
    ui->compass_1->addDial();
    ui->compass_1->dials->at(0)->setColor(QColor(255,100,70));
    ui->compass_1->dials->at(0)->setVal(0);

    ui->compass_1->dials->at(0)->setEndAngle(180);


    ui->compass_2->setTop0(true);
    ui->compass_2->setTickInterval(10);
    ui->compass_2->setTickBigInterval(90);
    ui->compass_2->addDial();
    ui->compass_2->dials->at(0)->setColor(QColor(255,100,70));
    ui->compass_2->dials->at(0)->setMaxVal(0);
    ui->compass_2->dials->at(0)->setFilled(false);

    ui->compass_2->setTitle("Heading");
    ui->compass_2->setUndertext("");


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_horizontalSlider_valueChanged(int value)
{
    ui->widget->dials->at(0)->setVal(value);
    ui->widget->dials->at(2)->setStartAngle(ui->widget->dials->at(0)->getStartAngle() + ui->widget->dials->at(0)->getActAngle());
    ui->widget->dials->at(2)->setEndAngle(ui->widget->dials->at(2)->getStartAngle()+30);
    ui->widget->setValue(value);
}

void MainWindow::on_horizontalSlider_2_valueChanged(int value)
{
    ui->widget->dials->at(1)->setVal(value);
}

void MainWindow::on_horizontalSlider_3_valueChanged(int value)
{
    ui->widget->dials->at(2)->setVal(value);
}

void MainWindow::on_horizontalSlider_4_valueChanged(int value)
{
    ui->widget_2->dials->at(0)->setVal(value);
    ui->widget_2->setValue(value);
}

void MainWindow::on_horizontalSlider_5_valueChanged(int value)
{
    ui->compass_2->setValue(360 - value);
    ui->compass_0->setTickStartAngle(value);
    ui->compass_0->setTickEndAngle(value+360);
    ui->compass_0->dials->at(0)->setStartAngle(value);

    int globalangle = ui->compass_0->movableDial->getVal()-ui->compass_0->dials->at(0)->getStartAngle();

    if (globalangle > 360) globalangle = globalangle -360;
    if (globalangle < 0) globalangle = globalangle +360;

    ui->newHeading_2->setValue(globalangle);

    int newHeading  = ui->compass_0->movableDial->getVal() + (value-lastVal);
    if (newHeading > 360) newHeading = newHeading -360;
    if (newHeading < 0) newHeading = newHeading +360;

    ui->compass_0->movableDial->setVal(newHeading);

    lastVal = value;
}


void MainWindow::on_horizontalSlider_5_sliderReleased()
{
    /*int globalangle = ui->compass_0->movableDial->getVal()+ui->compass_0->dials->at(0)->getStartAngle();

    if (globalangle > 360) globalangle = globalangle -360;
    if (globalangle < 0) globalangle = globalangle +360;*/
    //ui->compass_0->movableDial->setVal(ui->compass_0->movableDial->getVal()+ui->compass_0->dials->at(0)->getStartAngle());
}
