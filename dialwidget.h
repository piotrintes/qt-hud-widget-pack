#ifndef DIAL_H
#define DIAL_H

#include <QWidget>
#include <QList>
#include <QString>

class Dial: public QObject
{
    Q_OBJECT
    QWidget *parent = nullptr;
    int startAngle=0;
    int endAngle=360;
    int actAngle=0;
    int minVal=0;
    int maxVal=100;
    int val=0;
    QColor color = Qt::blue;
    bool filled = true;
public:
    int getStartAngle() const;
    void setStartAngle(int value);
    int getEndAngle() const;
    void setEndAngle(int value);
    int getMinVal() const;
    void setMinVal(int value);
    int getMaxVal() const;
    void setMaxVal(int value);
    int getVal() const;
    void setVal(int value);
    void setValFromAngle(int value);
    QColor getColor() const;
    void setColor(const QColor &value);
    int getActAngle() const;
    QWidget *getParent() const;
    void setParent(QWidget *value);
    bool getFilled() const;
    void setFilled(bool value);

signals:
    void valueChanged();
};

class DialWidget: public QWidget
{
    Q_OBJECT

    int dialWidth = 30;

    bool displayValue = true;
    bool displayTitle = true;
    bool displayUndertext = true;
    int value = 0;
    QString title = "DialWidget";
    QString undertext = "tekst";
    QString valAdd = "";

    bool isClicked = false;                                     //Check is button is being clicked or not
    int pressX;                                                 //Cursor position on click/move
    int pressY;                                                 //Cursor position on click/move

    int tickInterval=0;
    int tickBigInterval=0;
    int tickStartAngle=0;
    int tickEndAngle=360;
    float tickAngleToVal=1;

    bool top0 = false;

public:

    QList<Dial*> *dials = new QList<Dial*>;
    Dial *movableDial = nullptr;

    DialWidget(QWidget *parent);
    void addDial();
    void setDisplaysVisible(bool title, bool undertext, bool value);

    int getDialWidth() const;
    void setDialWidth(int value);
    int getValue() const;
    void setValue(int value);
    QString getTitle() const;
    void setTitle(const QString &value);
    QString getUndertext() const;
    void setUndertext(const QString &value);
    bool getDisplayValue() const;
    void setDisplayValue(bool value);
    bool getDisplayTitle() const;
    void setDisplayTitle(bool value);
    bool getDisplayUndertext() const;
    void setDisplayUndertext(bool value);

    QString getValAdd() const;
    void setValAdd(const QString &value);

    int getTickInterval() const;
    void setTickInterval(int value);

    int getTickBigInterval() const;
    void setTickBigInterval(int value);

    int getTickStartAngle() const;
    void setTickStartAngle(int value);

    int getTickEndAngle() const;
    void setTickEndAngle(int value);

    float getTickAngleToVal() const;
    void setTickAngleToVal(float value);

    bool getTop0() const;
    void setTop0(bool value);

protected:
    virtual void paintEvent(QPaintEvent *event);                //Overloaded paint event
    virtual void mousePressEvent(QMouseEvent * event);          //Overloaded mouse press event (set isClicked = true)
    virtual void mouseReleaseEvent(QMouseEvent * event);        //Overloaded mouse relese event (set isClicked = false)
    virtual void mouseMoveEvent(QMouseEvent *event);
};

#endif // DIAL_H
